package ru.leorikwhite.decomposenavappstudy.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.ExampleListComponent

@Composable
fun ExampleListUi(component:ExampleListComponent){
    Column(
        modifier = Modifier.fillMaxSize()
            .background(Color.DarkGray)
    ){

    }
}