package ru.leorikwhite.decomposenavappstudy.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.arkivanov.decompose.extensions.compose.stack.Children
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.ExampleListComponent
import ru.leorikwhite.decomposenavappstudy.components.newexamplecomponent.NewExampleComponent

@Composable
fun NewExampleUi(component: NewExampleComponent){
    val childStack by component.childStack.collectAsState()

    //Отображается UI нужного экрана в зависимости от типа компонента
    Children(stack = childStack) {child ->
        when (val instance = child.instance) {
            is NewExampleComponent.Child.List -> ExampleListUi(instance.component)
            is NewExampleComponent.Child.Details -> ExampleDetailsUi(instance.component)
        }

    }
}