package ru.leorikwhite.decomposenavappstudy.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.detailcomponentexample.ExampleDetailsComponent

@Composable
fun ExampleDetailsUi (component:ExampleDetailsComponent){
    Column(
        modifier = Modifier.fillMaxSize()
            .background(Color.Red)
    ) {

    }
}