package ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.detailcomponentexample

import kotlinx.coroutines.flow.StateFlow

interface ExampleDetailsComponent {

    val detailsState: StateFlow<String>

}