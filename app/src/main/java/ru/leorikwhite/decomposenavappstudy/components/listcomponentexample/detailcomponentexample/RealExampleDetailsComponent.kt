package ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.detailcomponentexample

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class RealExampleDetailsComponent(
    componentContext: ComponentContext
): ComponentContext by componentContext, ExampleDetailsComponent {

    override val detailsState: StateFlow<String> = MutableStateFlow("")

}