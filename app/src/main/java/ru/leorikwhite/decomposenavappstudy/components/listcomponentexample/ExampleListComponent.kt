package ru.leorikwhite.decomposenavappstudy.components.listcomponentexample

import kotlinx.coroutines.flow.StateFlow

interface ExampleListComponent {

    val exampleListState: StateFlow<List<String>>

    fun onElementClick(exampleId:Int)

}