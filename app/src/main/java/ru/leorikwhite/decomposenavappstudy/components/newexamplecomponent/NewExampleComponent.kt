package ru.leorikwhite.decomposenavappstudy.components.newexamplecomponent

import com.arkivanov.decompose.Child
import com.arkivanov.decompose.router.stack.ChildStack
import kotlinx.coroutines.flow.StateFlow
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.ExampleListComponent
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.detailcomponentexample.ExampleDetailsComponent

interface NewExampleComponent {

    val childStack: StateFlow<ChildStack<*, Child>>

    sealed interface Child {
        class List(val component:ExampleListComponent) : Child
        class Details(val component:ExampleDetailsComponent) : Child
    }
}