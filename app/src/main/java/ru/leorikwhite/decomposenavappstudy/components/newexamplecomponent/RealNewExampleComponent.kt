package ru.leorikwhite.decomposenavappstudy.components.newexamplecomponent

import android.os.Parcelable
import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.router.stack.StackNavigation
import com.arkivanov.decompose.router.stack.childStack
import com.arkivanov.decompose.router.stack.push
import kotlinx.coroutines.flow.StateFlow
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.KSerializer
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.RealExampleListComponent
import ru.leorikwhite.decomposenavappstudy.components.listcomponentexample.detailcomponentexample.RealExampleDetailsComponent

class RealNewExampleComponent(
    componentContext:ComponentContext
): ComponentContext by componentContext, NewExampleComponent {


    //Позволяет манипулировать стеком конфигурации
    private val navigation = StackNavigation<ChildConfig>()

    //Создает стек навигации возвращает Value<ChildStack>, тип из Decompose
    override val childStack: StateFlow<ChildStack<*, NewExampleComponent.Child>> = childStack(
        source = navigation,

        //Начальное состояние стека задается initialConfiguration
        initialConfiguration = ChildConfig.List,
        //Обработка кнопки назад, стек сам обрабатывает нажатие ии удаляет элемент с вершины стека
        handleBackButton = true,
        childFactory = ::createChild
    ).toStateFlow(lifecycle)


    //Фабрика компонентов, принимает конфигурацию и возвращает сам элемент
    private fun createChild(
        config: ChildConfig,
        componentContext: ComponentContext
    ) : NewExampleComponent.Child = when (config) {
        is ChildConfig.List -> {
            NewExampleComponent.Child.List(
                RealExampleListComponent(componentContext,
                    onElementChosen = {elementId ->
                        navigation.push(ChildConfig.Details(elementId))
                    })
            )
        }
        is ChildConfig.Details -> {
            NewExampleComponent.Child.Details(
                RealExampleDetailsComponent(componentContext)
            )
        }
    }

    //Конфиг компонента, хранится в постоянной памяти и нужен для востановления данных и для создания элемента
    private sealed interface ChildConfig : Parcelable {

        @Parcelize
        object List:ChildConfig

        @Parcelize
        data class Details(val elementId : Int) : ChildConfig

    }

}