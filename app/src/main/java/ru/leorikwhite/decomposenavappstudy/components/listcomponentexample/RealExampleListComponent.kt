package ru.leorikwhite.decomposenavappstudy.components.listcomponentexample

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class RealExampleListComponent(
    componentContext: ComponentContext,
    val onElementChosen:(Int)->Unit
) : ComponentContext by componentContext, ExampleListComponent{

    override val exampleListState: StateFlow<List<String>> = MutableStateFlow(listOf())

    override fun onElementClick(exampleId: Int) {
        onElementChosen(exampleId)
    }

}